//
//  WeatherOutsideUITests.swift
//  WeatherOutsideUITests
//
//  Created by Wen-lung Chang on 2018/11/21.
//  Copyright © 2018 Calvin. All rights reserved.
//

import XCTest

class WeatherOutsideUITests: XCTestCase {
    
    let app = XCUIApplication()

    override func setUp() {
        // Put setup code here. This method is called before the invocation of each test method in the class.

        // In UI tests it is usually best to stop immediately when a failure occurs.
        continueAfterFailure = false

        app.launchArguments.append("--uitesting")

        // UI tests must launch the application that they test. Doing this in setup will make sure it happens for each test method.
        app.launch()

        // In UI tests it’s important to set the initial state - such as interface orientation - required for your tests before they run. The setUp method is a good place to do this.
    }

    override func tearDown() {
        // Put teardown code here. This method is called after the invocation of each test method in the class.
    }

    func testTabChange() {
        let tabBarsQuery = XCUIApplication().tabBars

        if waitForElementToAppear(app.images["CurrentWeatherIcon"]) {
            XCTAssertTrue(app.images["CurrentWeatherIcon"].exists)
            
            tabBarsQuery.buttons["Forecast"].tap()
            if waitForElementToAppear(app.tables["ForecastTableView"].cells["cell01"]) {
                XCTAssertTrue(app.tables["ForecastTableView"].cells["cell01"].exists)

                tabBarsQuery.buttons["Today"].tap()
                XCTAssertTrue(app.images["CurrentWeatherIcon"].exists)
            }
        }
    }
    
    func testForecastScroll() {
        app.tabBars.buttons["Forecast"].tap()
        if waitForElementToAppear(app.tables["ForecastTableView"].cells["cell01"]) {

            while !app.tables["ForecastTableView"].cells["cell32"].visible() {
                app.swipeUp()
            }
            XCTAssertTrue(app.tables["ForecastTableView"].cells["cell32"].exists)
        }
    }
}
