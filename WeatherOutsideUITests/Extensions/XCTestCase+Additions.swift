//
//  XCTestCase+Additions.swift
//  WeatherOutsideUITests
//
//  Created by Wen-lung Chang on 2018/11/18.
//  Copyright © 2018 Calvin. All rights reserved.
//

import XCTest

func waitForElementToAppear(_ element: XCUIElement) -> Bool {
    let predicate = NSPredicate(format: "exists == true")
    let expectation = XCTNSPredicateExpectation(predicate: predicate,
                                                object: element)
    
    let result = XCTWaiter().wait(for: [expectation], timeout: 10)
    return result == .completed
}
