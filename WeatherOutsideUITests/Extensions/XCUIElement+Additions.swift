//
//  XCUIElement+Additions.swift
//  WeatherOutsideUITests
//
//  Created by Wen-lung Chang on 2018/11/18.
//  Copyright © 2018 Calvin. All rights reserved.
//

import XCTest

extension XCUIElement {
    
    func scrollToElement(element: XCUIElement) {
        while !element.visible() {
            swipeUp()
        }
    }
    
    func visible() -> Bool {
        guard self.exists && !self.frame.isEmpty else { return false }
        return XCUIApplication().windows.element(boundBy: 0).frame.contains(self.frame)
    }
}
