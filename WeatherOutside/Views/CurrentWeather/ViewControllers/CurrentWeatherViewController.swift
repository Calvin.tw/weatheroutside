//
//  CurrentWeatherViewController.swift
//  WeatherOutside
//
//  Created by Wen-lung Chang on 2018/11/20.
//  Copyright © 2018 Calvin. All rights reserved.
//

import UIKit
import Swinject
import SwinjectAutoregistration
import RxSwift
import RxCocoa

class CurrentWeatherViewController: UIViewController {

    // MARK: - xib properties
    @IBOutlet weak var locationDescriptionLabel: UILabel!

    @IBOutlet weak var weatherDescriptionLabel: UILabel!

    @IBOutlet weak var humidityLabel: UILabel!

    @IBOutlet weak var precipitationLabel: UILabel!

    @IBOutlet weak var pressureLabel: UILabel!

    @IBOutlet weak var windLabel: UILabel!

    @IBOutlet weak var windDirectionLabel: UILabel!

    @IBOutlet weak var shareButton: UIButton!

    @IBOutlet weak var iconImageView: UIImageView!
    
    // MARK: - private
    var viewModel: CurrentWeatherViewModeling?

    override func viewDidLoad() {
        super.viewDidLoad()

        configureView()
        bindViewModel()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        navigationController?.navigationBar.topItem?.title = "Today"
        NotificationCenter.default
            .addObserver(self,
                         selector: #selector(CurrentWeatherViewController.appWillEnterForeground),
                         name: UIApplication.willEnterForegroundNotification,
                         object: nil)
    }

    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)

        viewModel?.fetchWeatherData()
    }

    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        
        NotificationCenter.default
            .removeObserver(self,
                            name: UIApplication.willEnterForegroundNotification,
                            object: nil)
    }
    
    @objc func appWillEnterForeground() {
        viewModel?.fetchWeatherData()
    }
    
    fileprivate func configureView() {
        

        shareButton.rx.tap.subscribe(onNext: { [weak self] (_) in
            if let weather = self?.weatherDescriptionLabel.text, let location = self?.locationDescriptionLabel.text {
                let item = ShareItemSource(weatherDescription: weather, location: location)
                self?.present(UIActivityViewController(activityItems: [item], applicationActivities: nil), animated: true)
            }
            else {
                self?.messageToast(message: ErrorMessage.noInfo.description(), action: nil)
            }
        }).disposed(by: disposeBag)
    }

    fileprivate func bindViewModel() {
        guard let viewModel = viewModel else {
            return
        }

        (locationDescriptionLabel.rx.text <~ viewModel.locationDescription.map { $0 }).disposed(by: disposeBag)

        Observable.combineLatest(viewModel.temperature, viewModel.weatherDescription).subscribe(onNext: { [weak self] (tuple) in
            self?.weatherDescriptionLabel.text = String(format: "%d°C | %@", tuple.0, tuple.1.capitalized)
        }).disposed(by: disposeBag)

        (humidityLabel.rx.text <~ viewModel.humidity.map { "\($0) %" }).disposed(by: disposeBag)

        (precipitationLabel.rx.text <~ viewModel.precipitation.map { String(format: "%@ mm", $0) }).disposed(by: disposeBag)

        (pressureLabel.rx.text <~ viewModel.pressure.map { String(format: "%d hPa", $0) }).disposed(by: disposeBag)

        (windLabel.rx.text <~ viewModel.windSpeed.map { String(format: "%.1f km/h", $0) }).disposed(by: disposeBag)

        (windDirectionLabel.rx.text <~ viewModel.windDegree.map { $0 }).disposed(by: disposeBag)

        (iconImageView.rx.image <~ viewModel.weatherIconName.map { UIImage(named: $0) }).disposed(by: disposeBag)

        viewModel.errorMessage.subscribe(onNext: { (errorMessage) in
            self.messageToast(message: errorMessage, action: nil)
        }).disposed(by: disposeBag)
    }

}
