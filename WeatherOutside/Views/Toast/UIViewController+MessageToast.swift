//
//  UIViewController+MessageToast.swift
//  WeatherOutside
//
//  Created by Calvin Chang on 2018/11/17.
//  Copyright © 2018 CalvinChang. All rights reserved.
//

import UIKit
import RxCocoa
import RxSwift

extension UIViewController {

    fileprivate var hide: (UIView, CGRect)->() {
        return { (view, frame)->() in
            UIView.animate(withDuration: 0.3, animations: {
                view.frame = frame
            }, completion: { (complete) in
                view.removeFromSuperview()
            })
        }
    }

    func messageToast(message: String, color: UIColor = .red, isHide: Bool = true, action: (()->())?) {
        let size = CGSize(width: view.bounds.width - 20, height: 20)

        let hideRect = CGRect(origin: CGPoint(x: 10 , y: -size.height - 10), size: size)
        let showRect = CGRect(origin: CGPoint(x: 10 , y: 10), size: size)

        let messageButton = UIButton(frame: hideRect)
        messageButton.setTitle(message, for: .normal)
        messageButton.tintColor = .white
        messageButton.titleLabel?.font = UIFont(name: "ProximaNova-Semibold", size: 15)
        messageButton.backgroundColor = color
        messageButton.layer.cornerRadius = 3

        messageButton.rx.tap.subscribe(onNext: { [weak self] (_) in
            if let action = action {
                action()
                self?.hide(messageButton, hideRect)
            }
        }).disposed(by: disposeBag)

        view.addSubview(messageButton)

        UIView.animate(withDuration: 0.3) { messageButton.frame = showRect }

        if isHide {
            DispatchQueue.main.asyncAfter(deadline: .now() + 3) {
                self.hide(messageButton, hideRect)
            }
        }
    }
}
