//
//  TabBarController.swift
//  WeatherOutside
//
//  Created by Calvin Chang on 2018/11/20.
//  Copyright © 2018 Calvin. All rights reserved.
//

import UIKit

class TabBarController: UITabBarController {
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        configureView()
    }
    
    fileprivate func configureView() {
        let currentWeatherViewController = Router.shared.container.resolve(CurrentWeatherViewController.self)!
        currentWeatherViewController.tabBarItem = UITabBarItem(title: "Today", image: #imageLiteral(resourceName: "TodayInactive.png"), selectedImage: #imageLiteral(resourceName: "TodayActive.png"))
        let forecastViewController = Router.shared.container.resolve(ForecastViewController.self)!
        forecastViewController.tabBarItem = UITabBarItem(title: "Forecast", image: #imageLiteral(resourceName: "ForecastInactive.png"), selectedImage: #imageLiteral(resourceName: "ForecastActive.png"))

        tabBar.unselectedItemTintColor = #colorLiteral(red: 0.200000003, green: 0.200000003, blue: 0.200000003, alpha: 1)
        tabBar.barTintColor = UIColor.white

        viewControllers = [currentWeatherViewController, forecastViewController]

        navigationController?.navigationBar.barTintColor = UIColor.white
        if let font = UIFont(name: "ProximaNova-Semibold", size: 18) {
            navigationController?.navigationBar.titleTextAttributes = [.font: font]
        }
    }
}
