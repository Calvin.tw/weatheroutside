//
//  HourlyForecastCell.swift
//  WeatherOutside
//
//  Created by Calvin Chang on 2018/11/20.
//  Copyright © 2018 Calvin. All rights reserved.
//

import UIKit
import Swinject
import SwinjectAutoregistration
import RxSwift
import RxCocoa

class HourlyForecastCell: UITableViewCell {

    @IBOutlet weak var iconImageView: UIImageView!

    @IBOutlet weak var timeLabel: UILabel!

    @IBOutlet weak var descriptionLabel: UILabel!

    @IBOutlet weak var temperatureLabel: UILabel!

    var hourlyForecastData: HourlyForecastRealm? {
        didSet {
            if let data = hourlyForecastData {
                viewModel.configureForecastData(data: data)
            }
        }
    }

    fileprivate let cellDisposeBag = DisposeBag()

    fileprivate let viewModel: HourlyForecastViewModeling

    fileprivate let container = Container() { (c) in
        c.autoregister(HourlyForecastViewModeling.self, initializer: HourlyForecastViewModel.init)
    }

    override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
        viewModel = container.resolve(HourlyForecastViewModeling.self)!
        super.init(style: style, reuseIdentifier: reuseIdentifier)
    }

    required init?(coder aDecoder: NSCoder) {
        viewModel = container.resolve(HourlyForecastViewModeling.self)!
        super.init(coder: aDecoder)
    }

    override func awakeFromNib() {
        super.awakeFromNib()

        bindViewModel()

        if let data = hourlyForecastData {
            viewModel.configureForecastData(data: data)
        }
    }

    fileprivate func bindViewModel() {
        (iconImageView.rx.image <~ viewModel.weatherIconName.map { UIImage(named: "\($0)-s") } ).disposed(by: cellDisposeBag)

        (timeLabel.rx.text <~ viewModel.dataTime.map { $0 } ).disposed(by: cellDisposeBag)

        (descriptionLabel.rx.text <~ viewModel.weatherDescription.map { $0.capitalized } ).disposed(by: cellDisposeBag)

        (temperatureLabel.rx.text <~ viewModel.temperature.map { "\($0) °" } ).disposed(by: cellDisposeBag)
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(false, animated: false)
    }

    override func setHighlighted(_ highlighted: Bool, animated: Bool) {
        super.setHighlighted(false, animated: false)
    }
}
