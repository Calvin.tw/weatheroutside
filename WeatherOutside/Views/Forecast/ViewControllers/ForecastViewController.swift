//
//  ForecastViewController.swift
//  WeatherOutside
//
//  Created by Calvin Chang on 2018/11/20.
//  Copyright © 2018 Calvin. All rights reserved.
//

import UIKit
import Swinject
import SwinjectAutoregistration
import RxSwift
import RxCocoa

class ForecastViewController: UIViewController {

    // MARK: - define
    final let cellHeight: CGFloat = 90

    final let headerHeight: CGFloat = 45

    final let cellIdentifier = "hourlyForecastCellIdentifier"

    final let headerIdentifier = "DailyHeaderIdentifier"

    @IBOutlet weak var forecastTableView: UITableView!

    // MARK: - private properties
    var viewModel: ForecastViewModeling?

    override func viewDidLoad() {
        super.viewDidLoad()

        configureView()
        bindViewModel()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)

        navigationController?.navigationBar.topItem?.title = ""
        NotificationCenter.default
            .addObserver(self,
                         selector: #selector(ForecastViewController.appWillEnterForeground),
                         name: UIApplication.willEnterForegroundNotification,
                         object: nil)
    }

    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        
        viewModel?.fetchForecast()
    }

    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        
        NotificationCenter.default
            .removeObserver(self,
                            name: UIApplication.willEnterForegroundNotification,
                            object: nil)
    }
    
    @objc func appWillEnterForeground() {
        viewModel?.fetchForecast()
    }

    fileprivate func configureView() {
        forecastTableView.accessibilityIdentifier = "ForecastTableView"
        forecastTableView.delegate = self
        forecastTableView.dataSource = self
        forecastTableView.register(UINib(nibName: "HourlyForecastCell", bundle: nil), forCellReuseIdentifier: cellIdentifier)
        forecastTableView.register(UINib(nibName: "DailyHeaderView", bundle: nil), forHeaderFooterViewReuseIdentifier: headerIdentifier)
        forecastTableView.tableFooterView = UIView()
    }

    fileprivate func bindViewModel() {
        guard let viewModel = viewModel else {
            return
        }

        (forecastTableView.rx.reloadData(animation: true) <~ viewModel.reload).disposed(by: disposeBag)

        viewModel.cityName.subscribe(onNext: { [weak self] (cityName) in
            self?.navigationController?.navigationBar.topItem?.title = cityName
        }).disposed(by: disposeBag)

        viewModel.errorMessage.subscribe(onNext: { (errorMessage) in
            self.messageToast(message: errorMessage, action: nil)
        }).disposed(by: disposeBag)
    }
}

extension ForecastViewController: UITableViewDelegate {
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return cellHeight
    }

    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return headerHeight
    }
}

extension ForecastViewController: UITableViewDataSource {
    func numberOfSections(in tableView: UITableView) -> Int {
        return viewModel?.dailyForecasts.count ?? 0
    }

    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return viewModel?.dailyForecasts[section].hourlyForecasts.count ?? 0
    }

    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: cellIdentifier, for: indexPath) as! HourlyForecastCell
        cell.hourlyForecastData = viewModel?.dailyForecasts[indexPath.section].hourlyForecasts[indexPath.row]
        
        cell.accessibilityIdentifier = "cell\(indexPath.section)\(indexPath.row)"
        return cell
    }

    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        let header = tableView.dequeueReusableHeaderFooterView(withIdentifier: headerIdentifier) as! DailyHeaderView
        header.weekdayLabel.text = viewModel?.dailyForecasts[section].name.uppercased()

        return header
    }
}
