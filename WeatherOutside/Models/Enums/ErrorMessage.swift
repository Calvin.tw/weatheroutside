//
//  ErrorMessage.swift
//  WeatherOutside
//
//  Created by Wen-lung Chang on 2018/11/20.
//  Copyright © 2018 Calvin. All rights reserved.
//

import Foundation


enum ErrorMessage: String {
    case null = "The content is empty"
    case network = "Network error, please open wi-fi or mobile data"
    case location = "Can't get the location information, please open GPS"
    case noInfo = "Currently have no information, please wait"
    case noAuth = "Authentication fail, please try again later"
    
    func description() -> String {
        return NSLocalizedString(self.rawValue, comment: "")
    }
}
