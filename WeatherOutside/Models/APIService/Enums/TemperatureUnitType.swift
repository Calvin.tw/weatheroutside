//
//  TemperatureUnitType.swift
//  WeatherOutside
//
//  Created by Wen-lung Chang on 2018/11/18.
//  Copyright © 2018 Calvin. All rights reserved.
//

import Foundation

enum TemperatureUnitType: String {
    case celsius = "metric"
    case fahrenheit = "imperial"
    case kelvin = ""
}
