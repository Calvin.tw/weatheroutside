//
//  CurrentWeatherRequest.swift
//  WeatherOutside
//
//  Created by Wen-lung Chang on 2018/11/18.
//  Copyright © 2018 Calvin. All rights reserved.
//

import CoreLocation
import MapKit

class CurrentWeatherRequest: APIServiceRequesting {
    var apiKey: String?

    let location: CLLocationCoordinate2D

    init(location: CLLocationCoordinate2D) {
        self.location = location
    }

    func parameters() -> [String : Any]? {
        var parameters = [String : Any]()
        parameters["lat"] = location.latitude
        parameters["lon"] = location.longitude
        parameters["units"] = TemperatureUnitType.celsius.rawValue

        if let apiKey = apiKey {
            parameters["appid"] = apiKey
        }

        return parameters
    }
}
