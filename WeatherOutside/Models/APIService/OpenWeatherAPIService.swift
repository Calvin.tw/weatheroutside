//
//  OpenWeatherAPIService.swift
//  WeatherOutside
//
//  Created by Wen-lung Chang on 2018/11/18.
//  Copyright © 2018 Calvin. All rights reserved.
//

import Moya
import CoreLocation

class OpenWeatherAPIService: NSObject, OpenWeatherAPIServicing {
    var apiKey = "5305b1c3301ad176f5df6e7f9be43e20"

    #if DEBUG
    let verbose = true
    #else
    let verbose = false
    #endif

    var provider: MoyaProvider<OpenWeatherServiceType>?

    override init() {
        super.init()
        provider = MoyaProvider<OpenWeatherServiceType>(stubClosure: MoyaProvider.neverStub, plugins: [NetworkLoggerPlugin(verbose: verbose, responseDataFormatter: JSONResponseDataFormatter)])
    }

    func requestCurrentWeather(location: CLLocationCoordinate2D, complete: ((CurrentWeatherEntity) -> ())?, error: ((Error) -> ())?) {
        let request = CurrentWeatherRequest(location: location)
        request.apiKey = apiKey
        
        provider?.rx
            .request(.currentWeather(request))
            .map(CurrentWeatherEntity.self)
            .subscribe(onSuccess: complete, onError: error).disposed(by: disposeBag)
    }

    func requestForecast(location: CLLocationCoordinate2D, complete: ((ForecastEntity) -> ())?, error: ((Error) -> ())?) {
        let request = ForecastRequest(location: location)
        request.apiKey = apiKey
        
        provider?.rx
            .request(.forecast(request))
            .map(ForecastEntity.self)
            .subscribe(onSuccess: complete, onError: error).disposed(by: disposeBag)
    }

    func JSONResponseDataFormatter(_ data: Data) -> Data {
        do {
            let dataAsJSON = try JSONSerialization.jsonObject(with: data)
            let prettyData = try JSONSerialization.data(withJSONObject:dataAsJSON , options: .prettyPrinted)
            return prettyData
        } catch {
            return data
        }
    }
}

class OpenWeatherAPIStubService: NSObject, OpenWeatherAPIServicing {
    var apiKey = "5305b1c3301ad176f5df6e7f9be43e20"
    
    fileprivate let provider = MoyaProvider<OpenWeatherServiceType>(stubClosure: MoyaProvider.immediatelyStub)
    
    func requestCurrentWeather(location: CLLocationCoordinate2D, complete: ((CurrentWeatherEntity) -> ())?, error: ((Error) -> ())?) {
        let request = CurrentWeatherRequest(location: location)
        request.apiKey = apiKey
        
        provider.rx
            .request(.currentWeather(request))
            .map(CurrentWeatherEntity.self)
            .subscribe(onSuccess: complete, onError: error).disposed(by: disposeBag)
    }
    
    func requestForecast(location: CLLocationCoordinate2D, complete: ((ForecastEntity) -> ())?, error: ((Error) -> ())?) {
        let request = ForecastRequest(location: location)
        request.apiKey = apiKey
        
        provider.rx
            .request(.forecast(request))
            .map(ForecastEntity.self)
            .subscribe(onSuccess: complete, onError: error).disposed(by: disposeBag)
    }
}
