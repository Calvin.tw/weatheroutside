//
//  OpenWeatherServiceType.swift
//  WeatherOutside
//
//  Created by Wen-lung Chang on 2018/11/18.
//  Copyright © 2018 Calvin. All rights reserved.
//

import Moya

enum OpenWeatherServiceType: TargetType {
    private var openWeatherBase: String { return "https://api.openweathermap.org/data/2.5/" }

    case currentWeather(CurrentWeatherRequest)
    case forecast(ForecastRequest)

    /// The target's base `URL`.
    public var baseURL: URL {
        return URL(string: openWeatherBase)!
    }

    /// The path to be appended to `baseURL` to form the full `URL`.
    public var path: String {
        switch self {
        case .currentWeather:
            return "weather"
        case .forecast:
            return "forecast"
        }
    }

    /// The HTTP method used in the request.
    public var method: Moya.Method {
        return .get
    }

    /// Provides stub data for use in testing.
    public var sampleData: Data {
        switch self {
        case .currentWeather:
            return stubData(fileName: "CurrentWeatherStub", ofType: "json")
        case .forecast:
            return stubData(fileName: "ForecastStub", ofType: "json")
        }
    }

    /// The type of HTTP task to be performed.
    public var task: Task {
        switch self {
        case .currentWeather(let request):
            if let parameters = request.parameters() {
                return .requestParameters(parameters: parameters, encoding: URLEncoding.default)
            }
            else {
                return .requestPlain
            }
        case .forecast(let request):
            if let parameters = request.parameters() {
                return .requestParameters(parameters: parameters, encoding: URLEncoding.default)
            }
            else {
                return .requestPlain
            }
        }
    }

    /// The type of validation to perform on the request. Default is `.none`.
    public var validationType: ValidationType {
        return .customCodes(Array(200...299))
    }

    /// The headers to be used in the request.
    public var headers: [String: String]? {
        return nil
    }

    public func stubData(fileName: String, ofType: String) -> Data {
        do {
            if let path = Bundle.main.path(forResource: fileName, ofType: ofType) {
                if let data = try String(contentsOfFile: path).data(using: .utf8) {
                    return data
                }
            }
            return Data()
        } catch {
            return Data()
        }
    }
}
