//
//  APIServiceRequesting.swift
//  WeatherOutside
//
//  Created by Wen-lung Chang on 2018/11/18.
//  Copyright © 2018 Calvin. All rights reserved.
//

import Foundation

protocol APIServiceRequesting {
    var apiKey: String? { get }

    func parameters() -> [String : Any]?
}
