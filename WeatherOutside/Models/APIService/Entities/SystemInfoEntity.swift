//
//  SystemInfoEntity.swift
//  WeatherOutside
//
//  Created by Wen-lung Chang on 2018/11/18.
//  Copyright © 2018 Calvin. All rights reserved.
//

import Foundation

struct SystemInfoEntity: Codable {
    let country: String?
    
    enum CodingKeys: String, CodingKey {
        case country
    }
}
