//
//  WindsInfoEntity.swift
//  WeatherOutside
//
//  Created by Wen-lung Chang on 2018/11/18.
//  Copyright © 2018 Calvin. All rights reserved.
//

import Foundation

struct WindsInfoEntity: Codable {
    let speed: Double?
    let degree: Double?
    
    enum CodingKeys: String, CodingKey {
        case speed
        case degree = "deg"
    }
}
