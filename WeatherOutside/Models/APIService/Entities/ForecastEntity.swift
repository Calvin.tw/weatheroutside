//
//  ForecastEntity.swift
//  WeatherOutside
//
//  Created by Wen-lung Chang on 2018/11/18.
//  Copyright © 2018 Calvin. All rights reserved.
//

import Foundation

struct ForecastEntity: Codable {
    let list: [HourlyForecastEntity]?
    let city: CityInfoEntity?
    
    enum CodingKeys: String, CodingKey {
        case list
        case city
    }
}
