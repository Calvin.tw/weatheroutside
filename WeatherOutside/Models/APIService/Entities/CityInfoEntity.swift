//
//  CityInfoEntity.swift
//  WeatherOutside
//
//  Created by Wen-lung Chang on 2018/11/18.
//  Copyright © 2018 Calvin. All rights reserved.
//

import Foundation

struct CityInfoEntity: Codable {
    let id: Int
    let name: String?
    let country: String?
    
    enum CodingKeys: String, CodingKey {
        case id
        case name
        case country
    }
}
