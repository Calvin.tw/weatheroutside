//
//  HourlyForecastEntity.swift
//  WeatherOutside
//
//  Created by Wen-lung Chang on 2018/11/18.
//  Copyright © 2018 Calvin. All rights reserved.
//

import Foundation

struct HourlyForecastEntity: Codable {
    let dataTime: Int?
    let mainInfo: MainInfoEntity?
    let weatherInfo: [WeatherInfoEntity]?
    let windsInfo: WindsInfoEntity?
    let rainInfo: RainInfoEntity?
    let cloudInfo: CloudInfoEntity?
    
    enum CodingKeys: String, CodingKey {
        case dataTime = "dt"
        case mainInfo = "main"
        case weatherInfo = "weather"
        case windsInfo = "wind"
        case rainInfo = "rain"
        case cloudInfo = "clouds"
    }
}
