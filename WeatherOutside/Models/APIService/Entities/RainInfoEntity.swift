//
//  RainInfoEntity.swift
//  WeatherOutside
//
//  Created by Wen-lung Chang on 2018/11/18.
//  Copyright © 2018 Calvin. All rights reserved.
//

import Foundation

struct RainInfoEntity: Codable {
    let precipitation: Double?
    
    enum CodingKeys: String, CodingKey {
        case precipitation = "3h"
    }
}
