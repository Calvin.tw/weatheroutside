//
//  MainInfoEntity.swift
//  WeatherOutside
//
//  Created by Wen-lung Chang on 2018/11/18.
//  Copyright © 2018 Calvin. All rights reserved.
//

import Foundation

struct MainInfoEntity: Codable {
    let temperature: Double?
    let pressure: Double?
    let humidity: Int?
    let temperatureMax: Double?
    let temperatureMin: Double?
    
    enum CodingKeys: String, CodingKey {
        case temperature = "temp"
        case pressure
        case humidity
        case temperatureMax = "temp_max"
        case temperatureMin = "temp_min"
    }
}
