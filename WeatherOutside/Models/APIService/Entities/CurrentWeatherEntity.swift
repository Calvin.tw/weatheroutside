//
//  CurrentWeatherEntity.swift
//  WeatherOutside
//
//  Created by Wen-lung Chang on 2018/11/18.
//  Copyright © 2018 Calvin. All rights reserved.
//

import Foundation

struct CurrentWeatherEntity: Codable {
    let cityName: String?
    let id: Int
    let mainInfo: MainInfoEntity?
    let weatherInfo: [WeatherInfoEntity]?
    let windsInfo: WindsInfoEntity?
    let systemInfo: SystemInfoEntity?
    let rainInfo: RainInfoEntity?
    let cloudInfo: CloudInfoEntity?
    
    enum CodingKeys: String, CodingKey {
        case cityName = "name"
        case id
        case mainInfo = "main"
        case weatherInfo = "weather"
        case windsInfo = "wind"
        case systemInfo = "sys"
        case rainInfo = "rain"
        case cloudInfo = "clouds"
    }
}
