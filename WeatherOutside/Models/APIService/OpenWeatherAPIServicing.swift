//
//  OpenWeatherAPIServicing.swift
//  WeatherOutside
//
//  Created by Wen-lung Chang on 2018/11/18.
//  Copyright © 2018 Calvin. All rights reserved.
//

import MapKit
import CoreLocation
import Moya

protocol OpenWeatherAPIServicing {
    var apiKey: String { get }
    
    func requestCurrentWeather(location: CLLocationCoordinate2D, complete:((CurrentWeatherEntity)->())?, error: ((Error)->())?)
    
    func requestForecast(location: CLLocationCoordinate2D, complete:((ForecastEntity)->())?, error: ((Error)->())?)
}
