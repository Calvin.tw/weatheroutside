//
//  DailyForecastEntity.swift
//  WeatherOutside
//
//  Created by Calvin Chang on 2018/11/20.
//  Copyright © 2018 Calvin. All rights reserved.
//

import Foundation

struct DailyForecastEntity {
    var hourlyForecasts: [HourlyForecastRealm]
    
    var name: String
}
