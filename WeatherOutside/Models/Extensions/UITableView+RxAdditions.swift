//
//  UITableView+RxAdditions.swift
//  WeatherOutside
//
//  Created by Wen-lung Chang on 2018/11/16.
//  Copyright © 2018 CalvinChang. All rights reserved.
//

import UIKit
import RxSwift
import RxCocoa

extension Reactive where Base: UITableView {

    /// Bindable sink for `reloadData` property.
    public func reloadData(animation: Bool = false) -> Binder<()> {
        return Binder(self.base) { view, _ in
            if !animation {
                view.reloadData()
            }
            else {
                UIView.transition(with: view,
                                  duration: 0.35,
                                  options: .transitionCrossDissolve,
                                  animations: {
                                    // Scroll to top
                                    self.base.scrollRectToVisible(CGRect(x: 0, y: 0, width: 1, height: 1), animated: false)
                                    self.base.reloadData()
                })
            }
        }
    }
}
