//
//  Operator.swift
//  WeatherOutside
//
//  Created by Wen-lung Chang on 2018/11/17.
//  Copyright © 2018 CalvinChang. All rights reserved.
//

import RxSwift
import RxCocoa
import UIKit

precedencegroup RightPrecedence {
    associativity: right
    
    // Binds tighter than assignment but looser than everything else
    higherThan: AssignmentPrecedence
}

// Two way binding operator between two Control Properties in main thread, that's all it takes
infix operator <-> : DefaultPrecedence

// Two way binding operator between two Control Properties in main thread
infix operator <~> : DefaultPrecedence

// One way binding operator From Observable to Binder
infix operator <- : RightPrecedence

// One way binding operator From Observable to Binder and execute in main thread
infix operator <~ : RightPrecedence

func <-> <T>(bindingTarget: ControlProperty<T>, variable: ControlProperty<T>) -> Disposable {

    let bindToTarget = variable.asObservable()
        .bind(to: bindingTarget)
    let bindToVariable = bindingTarget.asObservable()
        .subscribe(onNext: { (value) in
            variable.onNext(value)
        }) {
            bindToTarget.dispose()
    }

    return Disposables.create(bindToTarget, bindToVariable)
}

func <~> <T>(bindingTarget: ControlProperty<T>, variable: ControlProperty<T>) -> Disposable {
    let bindToTarget = variable.asObservable()
        .observeOn(MainScheduler.instance)
        .bind(to: bindingTarget)
    let bindToVariable = bindingTarget.asObservable()
        .observeOn(MainScheduler.instance)
        .subscribe(onNext: { (value) in
            variable.onNext(value)
        }) {
            bindToTarget.dispose()
    }

    return Disposables.create(bindToTarget, bindToVariable)
}

func <~> <T>(bindingTarget: ControlProperty<T>, variable: BehaviorRelay<T>) -> Disposable {
    let bindToVariable = bindingTarget.skipUntil(variable).subscribe(onNext: { (value) in
        variable.accept(value)
    })
    let bindToTarget = variable.asObservable()
        .observeOn(MainScheduler.instance)
        .subscribe(onNext: { (value) in
            bindingTarget.onNext(value)
        })
    return Disposables.create(bindToTarget, bindToVariable)
}

func <~> <T>(bindingTarget: ControlProperty<T?>, variable: BehaviorRelay<T>) -> Disposable {
    let bindToVariable = bindingTarget.skipUntil(variable).subscribe(onNext: { (value) in
        variable.accept(value!)
    })
    let bindToTarget = variable.asObservable()
        .observeOn(MainScheduler.instance)
        .subscribe(onNext: { (value) in
            bindingTarget.onNext(value)
        })
    return Disposables.create(bindToTarget, bindToVariable)
}

// MARK: - <-
func <- <T>(bindingTarget: Binder<T>, variable: Observable<T>) -> Disposable {
    let bindDisposable = variable.asObservable()
        .bind(to: bindingTarget)

    return bindDisposable
}

func <- <T>(bindingTarget: BehaviorRelay<T>, varibale: Observable<T>) -> Disposable {
    let bindDisposable = varibale.asObservable()
        .bind(to: bindingTarget)
    return bindDisposable
}

func <- <T>(bindingTarget: PublishRelay<T>, varibale: Observable<T>) -> Disposable {
    let bindDisposable = varibale.asObservable()
        .bind(to: bindingTarget)
    return bindDisposable
}

func <- <T>(bindingTarget: PublishRelay<T>, variable: BehaviorRelay<T>) -> Disposable {
    let bindDisposable = variable.bind(to: bindingTarget)
    bindingTarget.accept(variable.value)

    return bindDisposable
}

func <- <T>(bindingTarget: BehaviorRelay<T>, variable: BehaviorRelay<T>) -> Disposable {
    let bindDisposable = variable.bind(to: bindingTarget)
    bindingTarget.accept(variable.value)

    return bindDisposable
}

// MARK: - <~

func <~ <T>(bindingTarget: Binder<T>, variable: Observable<T>) -> Disposable {
    let bindToUIDisposable = variable.asObservable()
        .observeOn(MainScheduler.instance)
        .bind(to: bindingTarget)
    return bindToUIDisposable
}

func <~ <T>(bindingTarget: Binder<T>?, variable: Observable<T>) -> Disposable {
    guard bindingTarget != nil else {
        return Disposables.create()
    }
    let bindToUIDisposable = variable.asObservable()
        .observeOn(MainScheduler.instance)
        .bind(to: bindingTarget!)
    return bindToUIDisposable
}

func <~ <T>(bindingTarget: Binder<T?>, variable: Observable<T>) -> Disposable {
    let bindToUIDisposable = variable.asObservable()
        .observeOn(MainScheduler.instance)
        .bind(to: bindingTarget)
    return bindToUIDisposable
}

func <~ <T>(bindingTarget: Binder<T>, variable: PublishRelay<T>) -> Disposable {
    let bindToUIDisposable = variable.asObservable()
        .observeOn(MainScheduler.instance)
        .bind(to: bindingTarget)
    return bindToUIDisposable
}

func <~ <T>(bindingTarget: Binder<T>?, variable: PublishRelay<T>) -> Disposable {
    guard bindingTarget != nil else {
        return Disposables.create()
    }
    let bindToUIDisposable = variable.asObservable()
        .observeOn(MainScheduler.instance)
        .bind(to: bindingTarget!)
    return bindToUIDisposable
}

func <~ <T>(bindingTarget: Binder<T?>, variable: PublishRelay<T>) -> Disposable {
    let bindToUIDisposable = variable.asObservable()
        .observeOn(MainScheduler.instance)
        .bind(to: bindingTarget)
    return bindToUIDisposable
}

func <~ <T>(bindingTarget: Binder<T>, variable: BehaviorRelay<T>) -> Disposable {
    let bindToUIDisposable = variable.asObservable()
        .observeOn(MainScheduler.instance)
        .bind(to: bindingTarget)
    return bindToUIDisposable
}

func <~ <T>(bindingTarget: Binder<T>?, variable: BehaviorRelay<T>) -> Disposable {
    guard bindingTarget != nil else {
        return Disposables.create()
    }
    let bindToUIDisposable = variable.asObservable()
        .observeOn(MainScheduler.instance)
        .bind(to: bindingTarget!)
    return bindToUIDisposable
}

func <~ <T>(bindingTarget: Binder<T?>, variable: BehaviorRelay<T>) -> Disposable {
    let bindToUIDisposable = variable.asObservable()
        .observeOn(MainScheduler.instance)
        .bind(to: bindingTarget)
    return bindToUIDisposable
}

func <~ <T>(bindingTarget: ControlProperty<T>, variable: Observable<T>) -> Disposable {
    let bindToUIDisposable = variable.asObservable()
        .observeOn(MainScheduler.instance)
        .bind(to: bindingTarget)
    return bindToUIDisposable
}

func <~ <T>(bindingTarget: ControlProperty<T>, variable: PublishRelay<T>) -> Disposable {
    let bindToUIDisposable = variable.asObservable()
        .observeOn(MainScheduler.instance)
        .bind(to: bindingTarget)
    return bindToUIDisposable
}

func <~ <T>(bindingTarget: ControlProperty<T>, variable: BehaviorRelay<T>) -> Disposable {
    let bindToUIDisposable = variable.asObservable()
        .observeOn(MainScheduler.instance)
        .bind(to: bindingTarget)
    return bindToUIDisposable
}
