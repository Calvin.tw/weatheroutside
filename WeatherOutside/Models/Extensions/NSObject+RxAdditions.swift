//
//  NSObject+RxAdditions.swift
//  WeatherOutside
//
//  Created by Wen-lung Chang on 2018/11/15.
//  Copyright © 2018 CalvinChang. All rights reserved.
//

import Foundation
import RxSwift

extension NSObject {
    var disposeBag: DisposeBag {
        get {
            if value(forKeyString: "disposeBag") == nil {
                setValue(DisposeBag(), forKeyString: "disposeBag")
            }
            return value(forKeyString: "disposeBag") as! DisposeBag
        }
        set {
            setValue(newValue, forKeyString: "disposeBag")
        }
    }
}

