//
//  RxRelay+Operator.swift
//  SoloLearnTest
//
//  Created by Wen-lung Chang on 2018/10/29.
//  Copyright © 2018 CalvinChang. All rights reserved.
//

import RxSwift
import RxCocoa

infix operator << : RightPrecedence

extension PublishRelay {
    static public func << (target: inout PublishRelay<Element>, event: Element) {
        target.accept(event)
    }

    static public func << (target: inout PublishRelay<Element?>, event: Element) {
        target.accept(event)
    }

    static public func << (target: inout PublishRelay<Element>, event: Element?) {
        if let event = event {
            target.accept(event)
        }
    }
}

extension BehaviorRelay {
    static public func << (target: inout BehaviorRelay<Element>, event: Element) {
        target.accept(event)
    }

    static public func << (target: inout BehaviorRelay<Element?>, event: Element) {
        target.accept(event)
    }
    
    static public func << (target: inout BehaviorRelay<Element>, event: Element?) {
        if let event = event {
            target.accept(event)
        }
    }
}

func << <T>(target: PublishRelay<T?>?, event: T) {
    target?.accept(event)
}

func << <T>(target: PublishRelay<T>?, event: T) {
    target?.accept(event)
}

func << <T>(target: BehaviorRelay<T>?, event: T) {
    target?.accept(event)
}

func << <T>(target: BehaviorRelay<T?>?, event: T) {
    target?.accept(event)
}

func << <T>(target: PublishRelay<T>?, event: T?) {
    if let event = event {
        target?.accept(event)
    }
}

func << <T>(target: PublishRelay<T?>?, event: T?) {
    target?.accept(event)
}

func << <T>(target: BehaviorRelay<T>?, event: T?) {
    if let event = event {
        target?.accept(event)
    }
}

func << <T>(target: BehaviorRelay<T?>?, event: T?) {
    target?.accept(event)
}
