//
//  FirebaseService.swift
//  WeatherOutside
//
//  Created by Calvin Chang on 2018/11/20.
//  Copyright © 2018 Calvin. All rights reserved.
//


import Firebase
import FirebaseDatabase
import FirebaseAuth

let uidKey = "userid"

class FirebaseService: FirebaseServicing {
    func authentication(completeHandler: (()->())?, errorHandler: ((Error)->())?) {
        Auth.auth().signInAnonymously() { (authResult, error) in
            if let error = error {
                errorHandler?(error)
            }
            else {
                UserDefaults.standard.set(authResult?.user.uid, forKey: uidKey)
                completeHandler?()
            }
        }
    }
    
    func storeUserData(user: UserEntity,completeHandler: (()->())?, errorHandler: ((Error)->())?) {
        let userDB = Database.database().reference().child("Users")
        if let userID = UserDefaults.standard.string(forKey: uidKey) {
            let userDic: NSDictionary = ["id": userID,
                                         "lat": user.location.latitude,
                                         "lon": user.location.longitude,
                                         "temperature": user.temperature]
            userDB.child(userID).setValue(userDic) { (error, ref) in
                if let error = error {
                    errorHandler?(error)
                }
                else {
                    print("[DEBUG] User id saved successfully!")
                    completeHandler?()
                }
            }
        }
    }
}

class FirebaseStubService: FirebaseServicing {
    func authentication(completeHandler: (() -> ())?, errorHandler: ((Error) -> ())?) {
        print("[TEST] Authentication success!")
        completeHandler?()
    }
    
    func storeUserData(user: UserEntity, completeHandler: (() -> ())?, errorHandler: ((Error) -> ())?) {
        print("[TEST] Store \(user) success!")
        completeHandler?()
    }
}

