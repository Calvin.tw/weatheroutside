//
//  FirebaseServicing.swift
//  WeatherOutside
//
//  Created by Calvin Chang on 2018/11/20.
//  Copyright © 2018 Calvin. All rights reserved.
//

import Foundation

protocol FirebaseServicing {
    func authentication(completeHandler: (()->())?, errorHandler: ((Error)->())?)
    
    func storeUserData(user: UserEntity,completeHandler: (()->())?, errorHandler: ((Error)->())?)
}
