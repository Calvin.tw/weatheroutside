//
//  UserEntity.swift
//  WeatherOutside
//
//  Created by Calvin Chang on 2018/11/20.
//  Copyright © 2018 Calvin. All rights reserved.
//

import Foundation
import CoreLocation

struct UserEntity {
    var location: CLLocationCoordinate2D

    var temperature: Int
}
