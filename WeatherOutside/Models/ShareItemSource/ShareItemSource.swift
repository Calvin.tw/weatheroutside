//
//  ShareItemSource.swift
//  WeatherOutside
//
//  Created by Calvin Chang on 2018/11/20.
//  Copyright © 2018 Calvin. All rights reserved.
//

import UIKit

class ShareItemSource: NSObject, UIActivityItemSource {

    let weatherDescription: String

    let location: String

    init(weatherDescription: String, location: String) {
        self.weatherDescription = weatherDescription
        self.location = location
        super.init()
    }

    func activityViewControllerPlaceholderItem(_ activityViewController: UIActivityViewController) -> Any {
        return "Here is the weather \(weatherDescription) in \(location)"
    }

    func activityViewController(_ activityViewController: UIActivityViewController, itemForActivityType activityType: UIActivity.ActivityType?) -> Any? {
        return "Here is the weather \(weatherDescription) in \(location)"
    }
}
