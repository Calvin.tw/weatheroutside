//
//  Router.swift
//  WeatherOutside
//
//  Created by Calvin Chang on 2018/11/21.
//  Copyright © 2018 Calvin. All rights reserved.
//

import Swinject

class Router {
    static let shared = Router()
    
    let container = Container() { (c) in
        c.autoregister(OpenWeatherAPIServicing.self, initializer: OpenWeatherAPIService.init)
        
        c.autoregister(LocationServicing.self, initializer: LocationService.init)
        
        c.autoregister(DataServicing.self, initializer: DataService.init)
        
        c.autoregister(FirebaseServicing.self, initializer: FirebaseService.init)
        
        c.autoregister(CurrentWeatherViewModeling.self, initializer: CurrentWeatherViewModel.init)
        
        c.autoregister(ForecastViewModeling.self, initializer: ForecastViewModel.init)
        
        // View
        c.register(CurrentWeatherViewController.self, factory: { (r) -> CurrentWeatherViewController in
            return CurrentWeatherViewController()
        }).initCompleted({ (r, c) in
            c.viewModel = r.resolve(CurrentWeatherViewModeling.self)
        }).inObjectScope(.container)
        
        c.register(ForecastViewController.self, factory: { (r) -> ForecastViewController in
            return ForecastViewController()
        }).initCompleted({ (r, c) in
            c.viewModel = r.resolve(ForecastViewModeling.self)
        }).inObjectScope(.container)
    }
    
    private init() {}
    
    
}
