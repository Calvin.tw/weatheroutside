//
//  LocationRealm.swift
//  WeatherOutside
//
//  Created by Calvin Chang on 2018/11/19.
//  Copyright © 2018 Calvin. All rights reserved.
//

import Realm
import RealmSwift
import CoreLocation

class LocationRealm: Object {
    @objc dynamic var uid: String?

    let lat = RealmOptional<Double>()

    let lon = RealmOptional<Double>()

    override static func primaryKey() -> String? {
        return "uid"
    }

    convenience init(uid: String, location: CLLocationCoordinate2D) {
        self.init()

        self.uid = uid
        lat.value = location.latitude
        lon.value = location.longitude
    }

    required init() {
        super.init()
    }

    required init(value: Any, schema: RLMSchema) {
        super.init(value: value, schema: schema)
    }

    required init(realm: RLMRealm, schema: RLMObjectSchema) {
        super.init(realm: realm, schema: schema)
    }
}
