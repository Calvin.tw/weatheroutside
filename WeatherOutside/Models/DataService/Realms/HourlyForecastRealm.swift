//
//  HourlyForecastRealm.swift
//  WeatherOutside
//
//  Created by Wen-lung Chang on 2018/11/19.
//  Copyright © 2018 Calvin. All rights reserved.
//

import Realm
import RealmSwift

class HourlyForecastRealm: Object {
    let dataTime = RealmOptional<Int>()

    @objc dynamic var weatherDescription: String?
    @objc dynamic var icon: String?

    let temperature = RealmOptional<Int>()
    let pressure = RealmOptional<Int>()
    let humidity = RealmOptional<Int>()
    let windSpeed = RealmOptional<Double>()
    let windDegree = RealmOptional<Double>()
    let precipitation = RealmOptional<Double>()
    let cloudless = RealmOptional<Int>()

    override static func primaryKey() -> String? {
        return "dataTime"
    }

    convenience init(entity: HourlyForecastEntity) {
        self.init()
        
        self.dataTime.value = entity.dataTime
        
        self.icon = entity.weatherInfo?.first?.icon
        
        self.weatherDescription = entity.weatherInfo?.first?.description
        
        // Round the temperature
        if let temperature = entity.mainInfo?.temperature {
            self.temperature.value = lround(temperature)
        }
        
        // Round the pressure
        if let pressure = entity.mainInfo?.pressure {
            self.pressure.value = lround(pressure)
        }
        
        self.humidity.value = entity.mainInfo?.humidity
        
        // From meter/sec to km/hour
        if let speed = entity.windsInfo?.speed {
            self.windSpeed.value = speed * 3.6
        }
        
        self.windDegree.value = entity.windsInfo?.degree
        
        self.precipitation.value = entity.rainInfo?.precipitation
        
        self.cloudless.value = entity.cloudInfo?.cloudless
    }

    required init() {
        super.init()
    }

    required init(value: Any, schema: RLMSchema) {
        super.init(value: value, schema: schema)
    }

    required init(realm: RLMRealm, schema: RLMObjectSchema) {
        super.init(realm: realm, schema: schema)
    }
}
