//
//  ForecastRealm.swift
//  WeatherOutside
//
//  Created by Wen-lung Chang on 2018/11/19.
//  Copyright © 2018 Calvin. All rights reserved.
//

import Realm
import RealmSwift

class ForecastRealm: Object {
    let hourlyForecasts = List<HourlyForecastRealm>()

    @objc dynamic var uid: String?
    @objc dynamic var cityName: String?
    @objc dynamic var country: String?

    override static func primaryKey() -> String? {
        return "uid"
    }

    convenience init(uid: String, entity: ForecastEntity) {
        self.init()
        
        self.uid = uid
        
        self.cityName = entity.city?.name
        
        self.country = entity.city?.country
    }

    required init() {
        super.init()
    }

    required init(value: Any, schema: RLMSchema) {
        super.init(value: value, schema: schema)
    }

    required init(realm: RLMRealm, schema: RLMObjectSchema) {
        super.init(realm: realm, schema: schema)
    }
}
