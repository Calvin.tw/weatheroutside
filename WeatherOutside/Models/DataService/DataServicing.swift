//
//  DataServicing.swift
//  WeatherOutside
//
//  Created by Wen-lung Chang on 2018/11/18.
//  Copyright © 2018 Calvin. All rights reserved.
//

import Foundation
import CoreLocation

protocol DataServicing {
    typealias Location = CLLocationCoordinate2D

    func fetchLocation(completeHandler: ((LocationRealm)->())?, errorHandler: ((LocationRealm?, Error)->())?)

    func fetchCurrentWeather(location: Location, updateHandler:((CurrentWeatherRealm)->())?, errorHandler: ((CurrentWeatherRealm?, Error)->())?)

    func fetchForecast(location: Location, updateHandler:((ForecastRealm)->())?, errorHandler: ((ForecastRealm?, Error)->())?)
}
