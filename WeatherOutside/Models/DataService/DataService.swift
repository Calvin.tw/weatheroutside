//
//  DataService.swift
//  WeatherOutside
//
//  Created by Calvin Chang on 2018/11/19.
//  Copyright © 2018 Calvin. All rights reserved.
//

import Realm
import RealmSwift
import CoreLocation

class DataService: DataServicing {
    
    fileprivate let apiService: OpenWeatherAPIServicing
    
    fileprivate let locationService: LocationServicing

    fileprivate let realm: Realm

    init(apiService: OpenWeatherAPIServicing, locationService: LocationServicing) {
        self.apiService = apiService
        self.locationService = locationService
        self.realm = try! Realm()
    }

    func fetchLocation(completeHandler: ((LocationRealm) -> ())?, errorHandler: ((LocationRealm?, Error) -> ())?) {
        
        guard let uid = UserDefaults.standard.string(forKey: uidKey) else {
            assertionFailure("[ERROR] Can't get uid")
            return
        }
        
        let previousLocation = self.realm.objects(LocationRealm.self).filter({ (realm) -> Bool in
            return realm.uid == uid
        }).first
        
        locationService.fetchLocation(completeHandler: { [weak self] (location) in
            let locationRealm = LocationRealm(uid: uid, location: location)
            do {
                try self?.realm.write {
                    self?.realm.add(locationRealm, update: true)
                }
                completeHandler?(locationRealm)
            }
            catch {
                errorHandler?(previousLocation, error)
            }
        }) { (error) in
            errorHandler?(previousLocation, error)
        }
        
    }

    func fetchCurrentWeather(location: Location, updateHandler: ((CurrentWeatherRealm) -> ())?, errorHandler: ((CurrentWeatherRealm?, Error) -> ())?) {

        guard let uid = UserDefaults.standard.string(forKey: uidKey) else {
            assertionFailure("[ERROR] Can't get uid")
            return
        }

        let currentWeather = self.realm.objects(CurrentWeatherRealm.self).filter({ (realm) -> Bool in
            return realm.uid == uid
        }).first

        apiService.requestCurrentWeather(location: location, complete: { [weak self] (entity) in
            let currentWeatherRealm = CurrentWeatherRealm(uid: uid, entity: entity)
            
            do {
                try self?.realm.write {
                    self?.realm.add(currentWeatherRealm, update: true)
                }
            }
            catch {
                errorHandler?(currentWeather, error)
            }

            updateHandler?(currentWeatherRealm)
            }, error: { (error) in
                errorHandler?(currentWeather, error)
        })
    }

    func fetchForecast(location: Location, updateHandler: ((ForecastRealm) -> ())?, errorHandler: ((ForecastRealm?, Error) -> ())?) {

        guard let uid = UserDefaults.standard.string(forKey: uidKey) else {
            assertionFailure("[ERROR] Can't get uid")
            return
        }

        let forecastRealm = self.realm.objects(ForecastRealm.self).filter({ (realm) -> Bool in
            return realm.uid == uid
        }).first

        apiService.requestForecast(location: location, complete: { [weak self] (entity) in
            let forecastRealm = ForecastRealm(uid: uid, entity: entity)
            do {
                for hourlyEntity in entity.list ?? [] {
                    let hourlyRealm = HourlyForecastRealm(entity: hourlyEntity)
                    try self?.realm.write {
                        self?.realm.add(hourlyRealm, update: true)
                    }
                    forecastRealm.hourlyForecasts.append(hourlyRealm)
                }

                try self?.realm.write {
                    self?.realm.add(forecastRealm, update: true)
                }

                // Remove old data
                try self?.realm.write {
                    if let oldData = self?.realm.objects(HourlyForecastRealm.self).filter("dataTime<\(entity.list?[0].dataTime ?? 0)") {
                        self?.realm.delete(oldData)
                    }
                }
            }
            catch {
                errorHandler?(forecastRealm, error)
            }

            updateHandler?(forecastRealm)
            }, error: { (error) in
                errorHandler?(forecastRealm, error)
        })
    }
}
