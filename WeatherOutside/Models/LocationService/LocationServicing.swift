//
//  LocationServicing.swift
//  WeatherOutside
//
//  Created by Wen-lung Chang on 2018/11/21.
//  Copyright © 2018 Calvin. All rights reserved.
//

import CoreLocation

protocol LocationServicing {
    typealias Location = CLLocationCoordinate2D
    
    func fetchLocation(completeHandler: ((Location)->())?, errorHandler: ((Error)->())?)
}
