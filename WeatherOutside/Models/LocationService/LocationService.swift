//
//  LocationService.swift
//  WeatherOutside
//
//  Created by Wen-lung Chang on 2018/11/21.
//  Copyright © 2018 Calvin. All rights reserved.
//

import CoreLocation

class LocationService: NSObject, LocationServicing {
    fileprivate let locationManager = CLLocationManager()

    fileprivate var locationCompleteHandler: ((Location) -> ())?

    fileprivate var locationErrorHandler: ((Error) -> ())?

    override init() {
        super.init()
        
        locationManager.delegate = self
        locationManager.desiredAccuracy = kCLLocationAccuracyBest
        locationManager.requestAlwaysAuthorization()
    }

    func fetchLocation(completeHandler: ((Location) -> ())?, errorHandler: ((Error) -> ())?) {
        locationCompleteHandler = completeHandler
        locationErrorHandler = errorHandler

        if CLLocationManager.locationServicesEnabled() {
            locationManager.startUpdatingLocation()
        }
    }
}

extension LocationService: CLLocationManagerDelegate {
    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        let userLocation:CLLocation = locations[0] as CLLocation
        
        manager.stopUpdatingLocation()
        
        let location = Location(latitude: userLocation.coordinate.latitude, longitude: userLocation.coordinate.longitude)

        locationCompleteHandler?(location)
    }
    
    func locationManager(_ manager: CLLocationManager, didFailWithError error: Error) {
        locationErrorHandler?(error)
    }
}

class LocationStubService: LocationServicing {
    func fetchLocation(completeHandler: ((Location) -> ())?, errorHandler: ((Error) -> ())?) {
        completeHandler?(Location(latitude: 25, longitude: 131))
    }
}
