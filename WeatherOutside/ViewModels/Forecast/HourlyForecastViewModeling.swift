//
//  HourlyForecastViewModeling.swift
//  WeatherOutside
//
//  Created by Calvin Chang on 2018/11/20.
//  Copyright © 2018 Calvin. All rights reserved.
//

import RxSwift
import RxCocoa

protocol HourlyForecastViewModeling {
    var weatherIconName: PublishRelay<String> { get }
    
    var dataTime: PublishRelay<String> { get }
    
    var temperature: PublishRelay<Int> { get }
    
    var weatherDescription: PublishRelay<String> { get }
    
    func configureForecastData(data: HourlyForecastRealm)
}
