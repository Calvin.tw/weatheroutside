//
//  ForecastViewModeling.swift
//  WeatherOutside
//
//  Created by Calvin Chang on 2018/11/20.
//  Copyright © 2018 Calvin. All rights reserved.
//

import RxSwift
import RxCocoa

protocol ForecastViewModeling {
    
    var cityName: PublishRelay<String> { get }
    
    var reload: PublishRelay<()> { get }
    
    var dailyForecasts: [DailyForecastEntity] { get }
    
    var errorMessage: PublishRelay<String> { get }
    
    func fetchForecast()
}
