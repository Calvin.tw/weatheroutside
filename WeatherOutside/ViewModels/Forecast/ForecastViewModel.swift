//
//  ForecastViewModel.swift
//  WeatherOutside
//
//  Created by Calvin Chang on 2018/11/20.
//  Copyright © 2018 Calvin. All rights reserved.
//

import RxCocoa
import RxSwift
import CoreLocation

class ForecastViewModel: ForecastViewModeling {
    
    fileprivate let dataService: DataServicing

    var cityName: PublishRelay<String> = PublishRelay()

    var reload: PublishRelay<()> = PublishRelay()

    var dailyForecasts: [DailyForecastEntity] = []

    var errorMessage: PublishRelay<String> = PublishRelay()

    init(dataService: DataServicing) {
        self.dataService = dataService
    }

    func fetchForecast() {
        dataService.fetchLocation(completeHandler: {  [weak self] (locationRealm) in

            self?.fetchNetworkData(locationRealm: locationRealm)

        }) { [weak self] (locationRealm, error) in
            #if DEBUG
            print("[ERROR] \(error)")
            #endif

            self?.errorMessage << ErrorMessage.location.description()
        }
    }

    fileprivate func fetchNetworkData(locationRealm: LocationRealm) {
        if let lat = locationRealm.lat.value, let lon = locationRealm.lon.value {
            let location = CLLocationCoordinate2D(latitude: lat, longitude: lon)

            dataService.fetchForecast(location: location, updateHandler: { [weak self] (forecastRealm) in
                self?.parseForecastData(forecastRealm: forecastRealm)
            }, errorHandler: { [weak self] (forecastRealm, error) in
                #if DEBUG
                print("[ERROR] \(error)")
                #endif

                if let forecastRealm = forecastRealm {
                    self?.parseForecastData(forecastRealm: forecastRealm)
                }
                self?.errorMessage << ErrorMessage.network.description()
            })
        }
    }

    fileprivate func parseForecastData(forecastRealm: ForecastRealm) {
        cityName << forecastRealm.cityName

        dailyForecasts = createDailyForecasts(hourlyForecasts: forecastRealm.hourlyForecasts.map({ $0 }))

        reload << ()
    }

    fileprivate func createDailyForecasts(hourlyForecasts: [HourlyForecastRealm]) -> [DailyForecastEntity] {
        var dailyForecasts: [DailyForecastEntity] = []
        for hourlyForecast in hourlyForecasts {
            if let dataTime = hourlyForecast.dataTime.value {
                let second = Double(dataTime)
                let date = Date(timeIntervalSince1970: second)

                let formatter = DateFormatter()
                formatter.dateFormat = "EEEE"

                // If the last day is equal than append
                if let dailyForecast = dailyForecasts.last, dailyForecast.name == formatter.string(from: date) {
                    dailyForecasts[dailyForecasts.count - 1].hourlyForecasts.append(hourlyForecast)
                }
                else {
                    let dailyForecast = DailyForecastEntity(hourlyForecasts: [hourlyForecast], name: formatter.string(from: date))
                    dailyForecasts.append(dailyForecast)
                }
            }
        }
        if dailyForecasts.count > 0 {
            dailyForecasts[0].name = "Today"
        }
        return dailyForecasts
    }

}
