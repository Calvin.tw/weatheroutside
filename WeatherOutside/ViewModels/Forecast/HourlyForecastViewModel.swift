//
//  HourlyForecastViewModel.swift
//  WeatherOutside
//
//  Created by Calvin Chang on 2018/11/20.
//  Copyright © 2018 Calvin. All rights reserved.
//

import RxCocoa
import RxSwift

class HourlyForecastViewModel: HourlyForecastViewModeling {
    var weatherIconName: PublishRelay<String> = PublishRelay()

    var dataTime: PublishRelay<String> = PublishRelay()

    var temperature: PublishRelay<Int> = PublishRelay()

    var weatherDescription: PublishRelay<String> = PublishRelay()

    func configureForecastData(data: HourlyForecastRealm) {
        weatherIconName << data.icon

        if let second = data.dataTime.value {
            dataTime << fetchTimeDescription(Double(second))
        }

        temperature << data.temperature.value

        weatherDescription << data.weatherDescription
    }

    fileprivate func fetchTimeDescription(_ second: TimeInterval) -> String {
        let date = Date(timeIntervalSince1970: second)
        let formatter = DateFormatter()
        formatter.dateFormat = "hh:mm"
        return formatter.string(from: date)
    }
}
