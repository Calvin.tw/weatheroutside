//
//  CurrentWeatherViewModeling.swift
//  WeatherOutside
//
//  Created by Calvin Chang on 2018/11/19.
//  Copyright © 2018 Calvin. All rights reserved.
//

import RxSwift
import RxCocoa

protocol CurrentWeatherViewModeling {

    var weatherIconName: PublishRelay<String> { get }

    var locationDescription: PublishRelay<String> { get }

    var temperature: PublishRelay<Int> { get }

    var weatherDescription: PublishRelay<String> { get }

    var pop: PublishRelay<Int> { get }

    var precipitation: PublishRelay<Double> { get }

    var pressure: PublishRelay<Int> { get }

    var humidity: PublishRelay<Int> { get }

    var windSpeed: PublishRelay<Double> { get }

    var windDegree: PublishRelay<String> { get }
    
    var errorMessage: PublishRelay<String> { get }

    func fetchWeatherData()
}
