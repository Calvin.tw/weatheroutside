//
//  CurrentWeatherViewModel.swift
//  WeatherOutside
//
//  Created by Calvin Chang on 2018/11/19.
//  Copyright © 2018 Calvin. All rights reserved.
//

import RxCocoa
import RxSwift
import CoreLocation

class CurrentWeatherViewModel: CurrentWeatherViewModeling {

    fileprivate let dataService: DataServicing
    
    fileprivate let firebaseService: FirebaseServicing

    var weatherIconName: PublishRelay<String> = PublishRelay()

    var locationDescription: PublishRelay<String> = PublishRelay()

    var temperature: PublishRelay<Int> = PublishRelay()

    var weatherDescription: PublishRelay<String> = PublishRelay()

    var pop: PublishRelay<Int> = PublishRelay()

    var precipitation: PublishRelay<Double> = PublishRelay()

    var pressure: PublishRelay<Int> = PublishRelay()

    var humidity: PublishRelay<Int> = PublishRelay()

    var windSpeed: PublishRelay<Double> = PublishRelay()

    var windDegree: PublishRelay<String> = PublishRelay()

    var errorMessage: PublishRelay<String> = PublishRelay()

    init(dataService: DataServicing, firebaseService: FirebaseServicing) {
        self.dataService = dataService
        self.firebaseService = firebaseService
    }

    func fetchWeatherData() {
        firebaseService.authentication(completeHandler: { [weak self] () in
            self?.dataService.fetchLocation(completeHandler: { (locationRealm) in
                
                self?.fetchNetworkData(locationRealm: locationRealm)
                
            }) { [weak self] (locationRealm, error) in
                #if DEBUG
                print("[ERROR] \(error)")
                #endif

                if let locationRealm = locationRealm {
                    self?.fetchNetworkData(locationRealm: locationRealm)
                }
                self?.errorMessage << ErrorMessage.location.description()
            }
        }) { [weak self] (error) in
            #if DEBUG
            print("[ERROR] \(error)")
            #endif

            self?.errorMessage << ErrorMessage.noAuth.description()
        }
    }
    
    fileprivate func fetchNetworkData(locationRealm: LocationRealm) {
        if let lat = locationRealm.lat.value, let lon = locationRealm.lon.value {
            let location = CLLocationCoordinate2D(latitude: lat, longitude: lon)
            dataService.fetchCurrentWeather(location: location, updateHandler: { [weak self] (currentWeatherRealm) in
                self?.parseCurrentWeatherData(currentWeatherRealm: currentWeatherRealm)
                
                // Store to firebase
                self?.firebaseService.storeUserData(user: UserEntity(location: location, temperature: currentWeatherRealm.temperature.value ?? 0)
                    , completeHandler: nil, errorHandler: { (error) in
                        #if DEBUG
                        print("[ERROR] \(error)")
                        #endif
                        
                        self?.errorMessage << ErrorMessage.noAuth.description()
                })
                }, errorHandler: { [weak self] (currentWeatherRealm, error) in
                    #if DEBUG
                    print("[ERROR] \(error)")
                    #endif
                    
                    if let currentWeatherRealm = currentWeatherRealm {
                        self?.parseCurrentWeatherData(currentWeatherRealm: currentWeatherRealm)
                    }
                    
                    self?.errorMessage << ErrorMessage.network.description()
            })
        }
        
    }
    
    fileprivate func parseCurrentWeatherData(currentWeatherRealm: CurrentWeatherRealm) {
        weatherIconName << currentWeatherRealm.icon
        
        temperature << currentWeatherRealm.temperature.value
        
        weatherDescription << currentWeatherRealm.weatherDescription
        
        locationDescription << "\(currentWeatherRealm.cityName ?? "No Data"), \(currentWeatherRealm.country ?? "No Data")"
        
        pop << currentWeatherRealm.cloudless.value
        
        precipitation << currentWeatherRealm.precipitation.value
        
        pressure << currentWeatherRealm.pressure.value
        
        humidity << currentWeatherRealm.humidity.value
        
        windSpeed << currentWeatherRealm.windSpeed.value
        
        if let degree = currentWeatherRealm.windDegree.value {
            windDegree << degreeToDescription(degree: degree)
        }
    }
    
    fileprivate func degreeToDescription(degree: Double) -> String {
        guard degree >= 0 && degree < 365 else {
            assertionFailure("[ERROR] Degree can't under 0 or more than 365: \(degree)")
            return ""
        }
        switch degree {
        case 0:
            return "N"
        case 90:
            return "E"
        case 180:
            return "S"
        case 270:
            return "W"
        case 1..<90:
            return "NE"
        case 91..<180:
            return "SE"
        case 181..<270:
            return "SW"
        case 271..<365:
            return "NW"
        default:
            assertionFailure("[ERROR] Degree can't under 0 or more than 365: \(degree)")
            return ""
        }
    }
}
