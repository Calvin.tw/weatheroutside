//
//  HourlyForecastViewModelSpec.swift
//  WeatherOutsideTests
//
//  Created by Calvin Chang on 2018/11/20.
//  Copyright © 2018 Calvin. All rights reserved.
//

import Quick
import Nimble
import RxCocoa
import RxSwift
import Realm
import RealmSwift
import Swinject
import SwinjectAutoregistration

@testable import WeatherOutside

class HourlyForecastViewModelSpec: QuickSpec {

    override func spec() {
        let fakeForecast = HourlyForecastRealm()

        beforeEach {
            fakeForecast.weatherDescription = "weather description"
            fakeForecast.temperature.value = 28
            fakeForecast.dataTime.value = 1542531600
            fakeForecast.icon = "10n"
        }

        describe("hourly forecast view model") {
            let viewModel = HourlyForecastViewModel()

            describe("configure hourlly forecast data", {

                it("get weather description normally", closure: {
                    var result: String?

                    viewModel.weatherDescription.subscribe(onNext: { (weatherDescription) in
                        result = weatherDescription
                    }).disposed(by: self.disposeBag)

                    viewModel.configureForecastData(data: fakeForecast)

                    expect(result).toEventually(equal("weather description"))
                })

                it("get time normally", closure: {
                    var result: String?

                    viewModel.dataTime.subscribe(onNext: { (dataTime) in
                        result = dataTime
                    }).disposed(by: self.disposeBag)

                    viewModel.configureForecastData(data: fakeForecast)

                    expect(result).toEventually(equal("05:00"))
                })

                it("get temperature normally", closure: {
                    var result: Int?

                    viewModel.temperature.subscribe(onNext: { (temperature) in
                        result = temperature
                    }).disposed(by: self.disposeBag)

                    viewModel.configureForecastData(data: fakeForecast)

                    expect(result).toEventually(equal(28))
                })

                it("get icon name normally", closure: {
                    var result: String?

                    viewModel.weatherIconName.subscribe(onNext: { (weatherIconName) in
                        result = weatherIconName
                    }).disposed(by: self.disposeBag)

                    viewModel.configureForecastData(data: fakeForecast)

                    expect(result).toEventually(equal("10n"))
                })
            })
        }
    }
}
