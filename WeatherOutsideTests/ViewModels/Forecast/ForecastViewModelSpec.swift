//
//  ForecastViewModelSpec.swift
//  WeatherOutsideTests
//
//  Created by Calvin Chang on 2018/11/20.
//  Copyright © 2018 Calvin. All rights reserved.
//

import Quick
import Nimble
import RxCocoa
import RxSwift
import Realm
import RealmSwift
import Swinject
import SwinjectAutoregistration

@testable import WeatherOutside

class ForecastViewModelSpec: QuickSpec {

    override func spec() {
        
        let container = Container() { (c) in
            c.autoregister(OpenWeatherAPIServicing.self, initializer: OpenWeatherAPIStubService.init)
            
            c.autoregister(LocationServicing.self, initializer: LocationStubService.init)
            
            c.autoregister(DataServicing.self, initializer: DataStubService.init)
            
            c.autoregister(ForecastViewModeling.self, initializer: ForecastViewModel.init)
        }
        
        beforeSuite {
            self.removeTestRealms()
        }
        
        afterSuite {
            self.removeTestRealms()
        }
        
        describe("forecast view model") {
            var viewModel: ForecastViewModeling?
            
            describe("fetch forecast data", {
                
                context("if network error but get location", {
                    
                    beforeEach {
                        container.autoregister(OpenWeatherAPIServicing.self, initializer: OpenWeatherAPIErrorService.init)
                        viewModel = container.resolve(ForecastViewModeling.self)
                        
                        // fetch data
                        viewModel?.fetchForecast()
                    }
                    
                    it("get offline data", closure: {
                        var result: String?
                        
                        viewModel?.cityName.subscribe(onNext: { (cityName) in
                            result = cityName
                        }).disposed(by: self.disposeBag)
                        
                        viewModel?.fetchForecast()
                        
                        expect(result).toEventually(equal("Taipei"))
                    })
                    
                    it("get error message", closure: {
                        var result: String?
                        
                        viewModel?.errorMessage.subscribe(onNext: { (errorMessage) in
                            result = errorMessage
                        }).disposed(by: self.disposeBag)
                        
                        viewModel?.fetchForecast()
                        
                        expect(result).toEventually(equal("Network error, please open wi-fi or mobile data"))
                    })
                    
                    afterEach {
                        container.autoregister(OpenWeatherAPIServicing.self, initializer: OpenWeatherAPIStubService.init)
                    }
                })
                
                context("if both network & location success", {
                    
                    beforeEach {
                        viewModel = container.resolve(ForecastViewModeling.self)
                    }
                    
                    it("get city name normally", closure: {
                        var result: String?
                        
                        viewModel?.cityName.subscribe(onNext: { (cityName) in
                            result = cityName
                        }).disposed(by: self.disposeBag)
                        
                        viewModel?.fetchForecast()
                        
                        expect(result).toEventually(equal("Taipei"))
                    })
                    
                    it("get reload event normally", closure: {
                        var result = false
                        
                        viewModel?.reload.subscribe(onNext: { (_) in
                            result = true
                        }).disposed(by: self.disposeBag)
                        
                        viewModel?.fetchForecast()
                        
                        expect(result).toEventually(beTrue())
                    })
                    
                    it("get reload data normally", closure: {
                        var result: [DailyForecastEntity]?
                        
                        viewModel?.reload.subscribe(onNext: { (_) in
                            result = viewModel?.dailyForecasts
                        }).disposed(by: self.disposeBag)
                        
                        viewModel?.fetchForecast()
                        
                        expect(result?.count).toEventually(equal(6))
                    })
                })
            })
        }
    }
    
    fileprivate func removeTestRealms() {
        do {
            let realm = try Realm()
            try realm.write {
                realm.deleteAll()
            }
        }
        catch {
            fail("Can't fetch default realm")
        }
    }
}
