//
//  CurrentWeatherViewModelSpec.swift
//  WeatherOutsideTests
//
//  Created by Calvin Chang on 2018/11/19.
//  Copyright © 2018 Calvin. All rights reserved.
//

import Quick
import Nimble
import RxCocoa
import RxSwift
import Realm
import RealmSwift
import Swinject
import SwinjectAutoregistration

@testable import WeatherOutside

class CurrentWeatherViewModelSpec: QuickSpec {
    override func spec() {

        let container = Container() { (c) in
            c.autoregister(OpenWeatherAPIServicing.self, initializer: OpenWeatherAPIStubService.init)
            
            c.autoregister(LocationServicing.self, initializer: LocationStubService.init)

            c.autoregister(DataServicing.self, initializer: DataStubService.init)
            
            c.autoregister(FirebaseServicing.self, initializer: FirebaseStubService.init)

            c.autoregister(CurrentWeatherViewModeling.self, initializer: CurrentWeatherViewModel.init)
        }

        beforeSuite {
            self.removeTestRealms()
        }

        afterSuite {
            self.removeTestRealms()
        }

        describe("current page view model") {
            var viewModel: CurrentWeatherViewModeling?

            describe("fetch current weather data", {

                context("if network error but get location", {

                    beforeEach {
                        viewModel = container.resolve(CurrentWeatherViewModeling.self)
                        viewModel?.fetchWeatherData()
                        
                        container.autoregister(OpenWeatherAPIServicing.self, initializer: OpenWeatherAPIErrorService.init)
                        viewModel = container.resolve(CurrentWeatherViewModeling.self)
                    }

                    it("get offline data", closure: {
                        var result: Int?

                        viewModel?.temperature.subscribe(onNext: { (temperature) in
                            result = temperature
                        }).disposed(by: self.disposeBag)

                        viewModel?.fetchWeatherData()

                        expect(result).toEventually(equal(24))
                    })

                    it("get error message", closure: {
                        var result: String?

                        viewModel?.errorMessage.subscribe(onNext: { (errorMessage) in
                            result = errorMessage
                        }).disposed(by: self.disposeBag)

                        viewModel?.fetchWeatherData()

                        expect(result).toEventually(equal("Network error, please open wi-fi or mobile data"))
                    })

                    afterEach {
                        container.autoregister(OpenWeatherAPIServicing.self, initializer: OpenWeatherAPIStubService.init)
                    }
                })

                context("if both network & location success", {

                    beforeEach {
                        viewModel = container.resolve(CurrentWeatherViewModeling.self)
                    }

                    it("get weather icon name normally", closure: {
                        var result: String?

                        viewModel?.weatherIconName.subscribe(onNext: { (weatherIconName) in
                            result = weatherIconName
                        }).disposed(by: self.disposeBag)

                        viewModel?.fetchWeatherData()

                        expect(result).toEventually(equal("10n"))
                    })

                    it("get location description normally", closure: {
                        var result: String?

                        viewModel?.locationDescription.subscribe(onNext: { (locationDescription) in
                            result = locationDescription
                        }).disposed(by: self.disposeBag)

                        viewModel?.fetchWeatherData()

                        expect(result).toEventually(equal("Taipei, TW"))
                    })

                    it("get temperature normally", closure: {
                        var result: Int?

                        viewModel?.temperature.subscribe(onNext: { (temperature) in
                            result = temperature
                        }).disposed(by: self.disposeBag)

                        viewModel?.fetchWeatherData()

                        expect(result).toEventually(equal(24))
                    })

                    it("get weather description normally", closure: {
                        var result: String?

                        viewModel?.weatherDescription.subscribe(onNext: { (weatherDescription) in
                            result = weatherDescription
                        }).disposed(by: self.disposeBag)

                        viewModel?.fetchWeatherData()

                        expect(result).toEventually(equal("light rain"))
                    })

                    it("get pop value normally", closure: {
                        var result: Int?

                        viewModel?.pop.subscribe(onNext: { (pop) in
                            result = pop
                        }).disposed(by: self.disposeBag)

                        viewModel?.fetchWeatherData()

                        expect(result).toEventually(equal(64))
                    })

                    it("get precipitation normally", closure: {
                        var result: Double?

                        viewModel?.precipitation.subscribe(onNext: { (precipitation) in
                            result = precipitation
                        }).disposed(by: self.disposeBag)

                        viewModel?.fetchWeatherData()

                        expect(result).toEventually(equal(0.395))
                    })

                    it("get pressure normally", closure: {
                        var result: Int?

                        viewModel?.pressure.subscribe(onNext: { (pressure) in
                            result = pressure
                        }).disposed(by: self.disposeBag)

                        viewModel?.fetchWeatherData()

                        expect(result).toEventually(equal(1019))
                    })

                    it("get humidity normally", closure: {
                        var result: Int?

                        viewModel?.humidity.subscribe(onNext: { (humidity) in
                            result = humidity
                        }).disposed(by: self.disposeBag)

                        viewModel?.fetchWeatherData()

                        expect(result).toEventually(equal(100))
                    })

                    it("get wind speed normally", closure: {
                        var result: Double?

                        viewModel?.windSpeed.subscribe(onNext: { (windSpeed) in
                            result = windSpeed
                        }).disposed(by: self.disposeBag)

                        viewModel?.fetchWeatherData()

                        expect(result).toEventually(equal(11.268))
                    })

                    it("get wind degree normally", closure: {
                        var result: String?

                        viewModel?.windDegree.subscribe(onNext: { (windDegree) in
                            result = windDegree
                        }).disposed(by: self.disposeBag)

                        viewModel?.fetchWeatherData()

                        expect(result).toEventually(equal("SW"))
                    })
                })
            })
        }
    }

    fileprivate func removeTestRealms() {
        do {
            let realm = try Realm()
            try realm.write {
                realm.deleteAll()
            }
        }
        catch {
            fail("Can't fetch default realm")
        }
    }
}
