//
//  DataStubService.swift
//  WeatherOutsideTests
//
//  Created by Calvin Chang on 2018/11/19.
//  Copyright © 2018 Calvin. All rights reserved.
//

import Realm
import RealmSwift

@testable import WeatherOutside

class DataStubService: DataServicing {

    fileprivate let apiService: OpenWeatherAPIServicing
    
    fileprivate let locationService: LocationServicing

    fileprivate let realm: Realm
    
    fileprivate let uid = "testKey"

    init(apiService: OpenWeatherAPIServicing, locationService: LocationServicing) {
        self.apiService = apiService
        self.locationService = locationService
        self.realm = try! Realm()
    }

    func fetchLocation(completeHandler: ((LocationRealm) -> ())?, errorHandler: ((LocationRealm?, Error) -> ())?) {
        let previousLocation = self.realm.objects(LocationRealm.self).first
        
        locationService.fetchLocation(completeHandler: { [weak self] (location) in
            let locationRealm = LocationRealm(uid: "testKey", location: location)
            do {
                try self?.realm.write {
                    self?.realm.add(locationRealm, update: true)
                }
            }
            catch {
                errorHandler?(previousLocation, error)
            }
            completeHandler?(locationRealm)
        }) { (error) in
            errorHandler?(previousLocation, error)
        }
    }

    func fetchCurrentWeather(location: DataStubService.Location, updateHandler: ((CurrentWeatherRealm) -> ())?, errorHandler: ((CurrentWeatherRealm?, Error) -> ())?) {
        let currentWeather = self.realm.objects(CurrentWeatherRealm.self).filter { (realm) -> Bool in
            return realm.uid == "testKey"
        }.first

        apiService.requestCurrentWeather(location: location, complete: { [weak self] (entity) in
            let currentWeatherRealm = CurrentWeatherRealm(uid: self?.uid ?? "testKey", entity: entity)
            do {
                try self?.realm.write {
                    self?.realm.add(currentWeatherRealm, update: true)
                }
            }
            catch {
                errorHandler?(currentWeather, error)
            }

            updateHandler?(currentWeatherRealm)
            }, error: { (error) in
                errorHandler?(currentWeather, error)
        })
    }
    
    func fetchForecast(location: DataStubService.Location, updateHandler: ((ForecastRealm) -> ())?, errorHandler: ((ForecastRealm?, Error) -> ())?) {
        let forecastRealm = self.realm.objects(ForecastRealm.self).filter { (realm) -> Bool in
            return realm.uid == "testKey"
            }.first

        apiService.requestForecast(location: location, complete: { [weak self] (entity) in
            let forecastRealm = ForecastRealm(uid: self?.uid ?? "testKey", entity: entity)
            do {
                for hourlyEntity in entity.list ?? [] {
                    let hourlyRealm = HourlyForecastRealm(entity: hourlyEntity)
                    try self?.realm.write {
                        self?.realm.add(hourlyRealm, update: true)
                    }
                    forecastRealm.hourlyForecasts.append(hourlyRealm)
                }

                try self?.realm.write {
                    self?.realm.add(forecastRealm, update: true)
                }
            }
            catch {
                errorHandler?(forecastRealm, error)
            }

            updateHandler?(forecastRealm)
            }, error: { (error) in
                errorHandler?(forecastRealm, error)
        })
    }
}
