//
//  DataServiceSpec.swift
//  WeatherOutsideTests
//
//  Created by Calvin Chang on 2018/11/19.
//  Copyright © 2018 Calvin. All rights reserved.
//

import Quick
import Nimble
import Realm
import RealmSwift
import Swinject
import SwinjectAutoregistration

@testable import WeatherOutside

class DataServiceSpec: QuickSpec {

    override func spec() {
        
        let container = Container() { (c) in
            c.autoregister(OpenWeatherAPIServicing.self, initializer: OpenWeatherAPIStubService.init)
            
            c.autoregister(LocationServicing.self, initializer: LocationStubService.init)
            
            c.autoregister(DataServicing.self, initializer: DataStubService.init)
        }
        
        var dataService: DataServicing?
        
        let location = DataServicing.Location(latitude: 20, longitude: 130)
        
        beforeSuite {
            self.removeTestRealms()
        }
        
        afterSuite {
            self.removeTestRealms()
        }

        describe("data service") {
            
            describe("fetch location", {

                context("if fail with offline data", {
                    beforeEach {
                        container.autoregister(LocationServicing.self, initializer: LocationStubService.init)

                        dataService = container.resolve(DataServicing.self)

                        waitUntil(action: { (done) in
                            dataService?.fetchLocation(completeHandler: { (_) in
                                done()
                            }, errorHandler: { (_, _) in
                                done()
                            })
                        })

                        container.autoregister(LocationServicing.self, initializer: LocationErrorService.init)

                        dataService = container.resolve(DataServicing.self)
                    }
                    
                    afterEach {
                        container.autoregister(LocationServicing.self, initializer: LocationStubService.init)
                    }
                    
                    it("fetch uid normally", closure: {
                        
                        var resultLocationRealm: LocationRealm?
                        
                        dataService?.fetchLocation(completeHandler: { (realm) in
                            fail("Should not success")
                        }, errorHandler: { (realm, error) in
                            resultLocationRealm = realm
                        })
                        
                        expect(resultLocationRealm?.uid).toEventually(equal("testKey"))
                    })
                })
                
                context("if fail without offline data", {
                    beforeEach {
                        self.removeTestRealms()
                        
                        container.autoregister(LocationServicing.self, initializer: LocationErrorService.init)
                        
                        dataService = container.resolve(DataServicing.self)
                    }
                    
                    afterEach {
                        container.autoregister(LocationServicing.self, initializer: LocationStubService.init)
                    }
                    
                    it("can't fetch uid normally", closure: {
                        
                        var resultLocationRealm: LocationRealm?
                        
                        dataService?.fetchLocation(completeHandler: { (realm) in
                            fail("Should not success")
                        }, errorHandler: { (realm, error) in
                            resultLocationRealm = realm
                        })
                        
                        expect(resultLocationRealm?.uid).toEventually(beNil())
                    })
                })

                context("fetch normally", {

                    beforeEach {
                        dataService = container.resolve(DataServicing.self)
                    }
                    
                    it("fetch location normally", closure: {
                        
                        var resultLocationRealm: LocationRealm?
                        
                        dataService?.fetchLocation(completeHandler: { (realm) in
                            resultLocationRealm = realm
                        }, errorHandler: { (realm, error) in
                            fail("Should not error")
                        })
                        
                        expect(resultLocationRealm?.lat.value).toEventually(equal(25))
                        expect(resultLocationRealm?.lon.value).toEventually(equal(131))
                    })
                })
            })
            
            describe("fetch current weather", {
                
                context("if fail with offline data", {
                    beforeEach {
                        dataService = container.resolve(DataServicing.self)
                        
                        dataService?.fetchCurrentWeather(location: location, updateHandler: nil, errorHandler: nil)
                        container.autoregister(OpenWeatherAPIServicing.self, initializer: OpenWeatherAPIErrorService.init)
                        
                        dataService = container.resolve(DataServicing.self)
                    }
                    
                    afterEach {
                        container.autoregister(OpenWeatherAPIServicing.self, initializer: OpenWeatherAPIStubService.init)
                    }
                    
                    it("fetch uid normally", closure: {
                        
                        var resultCurrentRealm: CurrentWeatherRealm?

                        dataService?.fetchCurrentWeather(location: location, updateHandler: { (realm) in
                            fail("Should not success")
                        }, errorHandler: { (realm, error) in
                            resultCurrentRealm = realm
                        })
                        
                        expect(resultCurrentRealm?.uid).toEventually(equal("testKey"))
                    })
                })
                
                context("if fail without offline data", {
                    beforeEach {
                        self.removeTestRealms()

                        container.autoregister(OpenWeatherAPIServicing.self, initializer: OpenWeatherAPIErrorService.init)
                        
                        dataService = container.resolve(DataServicing.self)
                    }
                    
                    afterEach {
                        container.autoregister(OpenWeatherAPIServicing.self, initializer: OpenWeatherAPIStubService.init)
                    }
                    
                    it("can't fetch uid normally", closure: {
                        
                        var resultCurrentRealm: CurrentWeatherRealm?
                        
                        dataService?.fetchCurrentWeather(location: location, updateHandler: { (realm) in
                            fail("Should not success")
                        }, errorHandler: { (realm, error) in
                            resultCurrentRealm = realm
                        })
                        
                        expect(resultCurrentRealm?.uid).toEventually(beNil())
                    })
                })
                
                
                context("fetch normally", {

                    beforeEach {
                        dataService = container.resolve(DataServicing.self)
                    }
                    
                    it("fetch uid normally", closure: {
                        var resultCurrentRealm: CurrentWeatherRealm?
                        
                        dataService?.fetchCurrentWeather(location: location, updateHandler: { (realm) in
                            resultCurrentRealm = realm
                        }, errorHandler: { (realm, error) in
                            fail("Should not error")
                        })
                        
                        expect(resultCurrentRealm?.uid).toEventually(equal("testKey"))
                    })
                    
                    it("fetch cityName normally", closure: {
                        var resultCurrentRealm: CurrentWeatherRealm?
                        
                        dataService?.fetchCurrentWeather(location: location, updateHandler: { (realm) in
                            resultCurrentRealm = realm
                        }, errorHandler: { (realm, error) in
                            fail("Should not error")
                        })
                        
                        expect(resultCurrentRealm?.cityName).toEventually(equal("Taipei"))
                    })
                    
                    it("fetch country normally", closure: {
                        var resultCurrentRealm: CurrentWeatherRealm?
                        
                        dataService?.fetchCurrentWeather(location: location, updateHandler: { (realm) in
                            resultCurrentRealm = realm
                        }, errorHandler: { (realm, error) in
                            fail("Should not error")
                        })
                        
                        expect(resultCurrentRealm?.country).toEventually(equal("TW"))
                    })
                    
                    it("fetch weather description normally", closure: {
                        var resultCurrentRealm: CurrentWeatherRealm?
                        
                        dataService?.fetchCurrentWeather(location: location, updateHandler: { (realm) in
                            resultCurrentRealm = realm
                        }, errorHandler: { (realm, error) in
                            fail("Should not error")
                        })
                        
                        expect(resultCurrentRealm?.weatherDescription).toEventually(equal("light rain"))
                    })
                    
                    it("fetch icon normally", closure: {
                        var resultCurrentRealm: CurrentWeatherRealm?
                        
                        dataService?.fetchCurrentWeather(location: location, updateHandler: { (realm) in
                            resultCurrentRealm = realm
                        }, errorHandler: { (realm, error) in
                            fail("Should not error")
                        })
                        
                        expect(resultCurrentRealm?.icon).toEventually(equal("10n"))
                    })
                    
                    it("fetch temperature normally", closure: {
                        var resultCurrentRealm: CurrentWeatherRealm?
                        
                        dataService?.fetchCurrentWeather(location: location, updateHandler: { (realm) in
                            resultCurrentRealm = realm
                        }, errorHandler: { (realm, error) in
                            fail("Should not error")
                        })
                        
                        expect(resultCurrentRealm?.temperature.value).toEventually(equal(24))
                    })
                    
                    it("fetch pressure normally", closure: {
                        var resultCurrentRealm: CurrentWeatherRealm?
                        
                        dataService?.fetchCurrentWeather(location: location, updateHandler: { (realm) in
                            resultCurrentRealm = realm
                        }, errorHandler: { (realm, error) in
                            fail("Should not error")
                        })
                        
                        expect(resultCurrentRealm?.pressure.value).toEventually(equal(1019))
                    })
                    
                    it("fetch humidity normally", closure: {
                        var resultCurrentRealm: CurrentWeatherRealm?
                        
                        dataService?.fetchCurrentWeather(location: location, updateHandler: { (realm) in
                            resultCurrentRealm = realm
                        }, errorHandler: { (realm, error) in
                            fail("Should not error")
                        })
                        
                        expect(resultCurrentRealm?.humidity.value).toEventually(equal(100))
                    })
                    
                    it("fetch windSpeed normally", closure: {
                        var resultCurrentRealm: CurrentWeatherRealm?
                        
                        dataService?.fetchCurrentWeather(location: location, updateHandler: { (realm) in
                            resultCurrentRealm = realm
                        }, errorHandler: { (realm, error) in
                            fail("Should not error")
                        })
                        
                        expect(resultCurrentRealm?.windSpeed.value).toEventually(equal(11.268))
                    })
                    
                    it("fetch windDegree normally", closure: {
                        var resultCurrentRealm: CurrentWeatherRealm?
                        
                        dataService?.fetchCurrentWeather(location: location, updateHandler: { (realm) in
                            resultCurrentRealm = realm
                        }, errorHandler: { (realm, error) in
                            fail("Should not error")
                        })
                        
                        expect(resultCurrentRealm?.windDegree.value).toEventually(equal(182.001))
                    })
                    
                    it("fetch precipitation normally", closure: {
                        var resultCurrentRealm: CurrentWeatherRealm?
                        
                        dataService?.fetchCurrentWeather(location: location, updateHandler: { (realm) in
                            resultCurrentRealm = realm
                        }, errorHandler: { (realm, error) in
                            fail("Should not error")
                        })
                        expect(resultCurrentRealm?.precipitation.value).toEventually(equal(0.395))
                    })
                    
                    it("fetch cloudless normally", closure: {
                        var resultCurrentRealm: CurrentWeatherRealm?
                        
                        dataService?.fetchCurrentWeather(location: location, updateHandler: { (realm) in
                            resultCurrentRealm = realm
                        }, errorHandler: { (realm, error) in
                            fail("Should not error")
                        })
                        
                        expect(resultCurrentRealm?.cloudless.value).toEventually(equal(64))
                    })
                })
            })
            
            describe("fetch forecast", {
                context("if fail with offline data", {
                    beforeEach {
                        dataService = container.resolve(DataServicing.self)
                        
                        dataService?.fetchForecast(location: location, updateHandler: nil, errorHandler: nil)
                        container.autoregister(OpenWeatherAPIServicing.self, initializer: OpenWeatherAPIErrorService.init)
                        
                        dataService = container.resolve(DataServicing.self)
                    }
                    
                    afterEach {
                        container.autoregister(OpenWeatherAPIServicing.self, initializer: OpenWeatherAPIStubService.init)
                    }
                    
                    it("fetch uid normally", closure: {
                        
                        var resultForecastRealm: ForecastRealm?
                        
                        dataService?.fetchForecast(location: location, updateHandler: { (realm) in
                            fail("Should not success")
                        }, errorHandler: { (realm, error) in
                            resultForecastRealm = realm
                        })
                        
                        expect(resultForecastRealm?.uid).toEventually(equal("testKey"))
                    })
                })
                
                context("if fail without offline data", {
                    beforeEach {
                        self.removeTestRealms()
                        
                        container.autoregister(OpenWeatherAPIServicing.self, initializer: OpenWeatherAPIErrorService.init)
                        
                        dataService = container.resolve(DataServicing.self)
                    }
                    
                    afterEach {
                        container.autoregister(OpenWeatherAPIServicing.self, initializer: OpenWeatherAPIStubService.init)
                    }
                    
                    it("can't fetch uid normally", closure: {
                        
                        var resultForecastRealm: ForecastRealm?
                        
                        dataService?.fetchForecast(location: location, updateHandler: { (realm) in
                            fail("Should not success")
                        }, errorHandler: { (realm, error) in
                            resultForecastRealm = realm
                        })
                        
                        expect(resultForecastRealm?.uid).toEventually(beNil())
                    })
                })
                
                context("fetch normally", {
                    
                    beforeEach {
                        dataService = container.resolve(DataServicing.self)
                    }

                    it("fetch city normally", closure: {
                        var resultForecastRealm: ForecastRealm?
                        
                        dataService?.fetchForecast(location: location, updateHandler: { (realm) in
                            resultForecastRealm = realm
                        }, errorHandler: { (realm, error) in
                            fail("Should not error")
                        })
                        
                        expect(resultForecastRealm?.cityName).toEventually(equal("Taipei"))
                    })
                    
                    it("fetch country normally", closure: {
                        var resultForecastRealm: ForecastRealm?
                        
                        dataService?.fetchForecast(location: location, updateHandler: { (realm) in
                            resultForecastRealm = realm
                        }, errorHandler: { (realm, error) in
                            fail("Should not error")
                        })
                        
                        expect(resultForecastRealm?.country).toEventually(equal("TW"))
                    })
                    
                    it("fetch hourly forecast normally", closure: {
                        var resultForecastRealm: ForecastRealm?
                        
                        dataService?.fetchForecast(location: location, updateHandler: { (realm) in
                            resultForecastRealm = realm
                        }, errorHandler: { (realm, error) in
                            fail("Should not error")
                        })
                        
                        expect(resultForecastRealm?.hourlyForecasts.count).toEventually(equal(37))
                    })
                })
            })
        }
    }
    
    fileprivate func removeTestRealms() {
        do {
            let realm = try Realm()
            try realm.write {
                realm.deleteAll()
            }
        }
        catch {
            fail("Can't fetch default realm")
        }
    }
}
