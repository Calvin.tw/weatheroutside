//
//  LocationStubService.swift
//  WeatherOutsideTests
//
//  Created by Wen-lung Chang on 2018/11/21.
//  Copyright © 2018 Calvin. All rights reserved.
//

import Foundation
import CoreLocation

@testable import WeatherOutside

class LocationStubService: LocationServicing {
    func fetchLocation(completeHandler: ((Location) -> ())?, errorHandler: ((Error) -> ())?) {
        completeHandler?(Location(latitude: 25, longitude: 131))
    }
}
