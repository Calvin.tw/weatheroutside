//
//  LocationErrorService.swift
//  WeatherOutsideTests
//
//  Created by Wen-lung Chang on 2018/11/21.
//  Copyright © 2018 Calvin. All rights reserved.
//

import Foundation
import CoreLocation

@testable import WeatherOutside

class LocationErrorService: LocationServicing {
    func fetchLocation(completeHandler: ((Location) -> ())?, errorHandler: ((Error) -> ())?) {
        errorHandler?(NSError(domain: "[TEST] Test Error", code: 0, userInfo: nil))
    }
}
