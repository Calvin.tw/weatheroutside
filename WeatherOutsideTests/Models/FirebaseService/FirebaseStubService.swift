//
//  FirebaseStubService.swift
//  WeatherOutsideTests
//
//  Created by Calvin Chang on 2018/11/20.
//  Copyright © 2018 Calvin. All rights reserved.
//

import Foundation

@testable import WeatherOutside

class FirebaseStubService: FirebaseServicing {
    func authentication(completeHandler: (() -> ())?, errorHandler: ((Error) -> ())?) {
        print("[TEST] Authentication success!")
        completeHandler?()
    }
    
    func storeUserData(user: UserEntity, completeHandler: (() -> ())?, errorHandler: ((Error) -> ())?) {
        print("[TEST] Store \(user) success!")
        completeHandler?()
    }
}
