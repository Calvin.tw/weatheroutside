//
//  OpenWeatherAPIErrorService.swift
//  WeatherOutsideTests
//
//  Created by Calvin Chang on 2018/11/19.
//  Copyright © 2018 Calvin. All rights reserved.
//

import Moya
import CoreLocation
import RxCocoa
import RxSwift

@testable import WeatherOutside

class OpenWeatherAPIErrorService: NSObject, OpenWeatherAPIServicing {
    var apiKey = "5305b1c3301ad176f5df6e7f9be43e20"
    
    fileprivate let provider = MoyaProvider<OpenWeatherServiceType>(endpointClosure: { (target: OpenWeatherServiceType) -> Endpoint in
        return Endpoint(url: URL(target: target).absoluteString,
                        sampleResponseClosure: { .networkResponse(500 , Data()) },
                        method: target.method,
                        task: target.task,
                        httpHeaderFields: target.headers)
    },stubClosure: MoyaProvider.immediatelyStub)
    
    func requestCurrentWeather(location: CLLocationCoordinate2D, complete: ((CurrentWeatherEntity) -> ())?, error: ((Error) -> ())?) {
        let request = CurrentWeatherRequest(location: location)
        request.apiKey = apiKey
        
        provider.rx
            .request(.currentWeather(request))
            .map(CurrentWeatherEntity.self)
            .subscribe(onSuccess: complete, onError: error).disposed(by: disposeBag)
    }
    
    func requestForecast(location: CLLocationCoordinate2D, complete: ((ForecastEntity) -> ())?, error: ((Error) -> ())?) {
        let request = ForecastRequest(location: location)
        request.apiKey = apiKey
        
        provider.rx
            .request(.forecast(request))
            .map(ForecastEntity.self)
            .subscribe(onSuccess: complete, onError: error).disposed(by: disposeBag)
    }
}
